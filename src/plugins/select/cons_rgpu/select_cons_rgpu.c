/*****************************************************************************\
 *  select_prueba.c - node selection plugin for prueba systems.
 *****************************************************************************
 *  Copyright (C) 2014 Universitat Jaume I and Universitat Politecnica de Valencia
 *  Written by Sergio Iserte <siserte@uji.es>
 *
 *  This file is part of SLURM, a resource management program.
 *  For details, see <http://slurm.schedmd.com/>.
 *  Please also read the included file: DISCLAIMER.
 *
 *  SLURM is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *
 *  In addition, as a special exception, the copyright holders give permission
 *  to link the code of portions of this program with the OpenSSL library under
 *  certain conditions as described in each individual source file, and
 *  distribute linked combinations including the two. You must obey the GNU
 *  General Public License in all respects for all of the code used other than
 *  OpenSSL. If you modify file(s) with this exception, you may extend this
 *  exception to your version of the file(s), but you are not obligated to do
 *  so. If you do not wish to do so, delete this exception statement from your
 *  version.  If you delete this exception statement from all source files in
 *  the program, then also delete it here.
 *
 *  SLURM is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with SLURM; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA.
\*****************************************************************************/

#ifdef HAVE_CONFIG_H
#  include "config.h"
#  if HAVE_STDINT_H
#    include <stdint.h>
#  endif
#  if HAVE_INTTYPES_H
#    include <inttypes.h>
#  endif
#endif

#include <stdio.h>
#include <sys/poll.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#include "src/common/slurm_xlator.h"	/* Must be first */
#include "src/common/pack.h"
#include "src/slurmctld/locks.h"
#include "other_select.h"

#include "gres.h"
#include "node_conf.h"

#define NODEINFO_MAGIC 0x82aa

/* These are defined here so when we link with something other than
 * the slurmctld we will have these symbols defined.  They will get
 * overwritten when linking with the slurmctld.
 */
#if defined (__APPLE__)
slurm_ctl_conf_t slurmctld_conf __attribute__((weak_import));
struct node_record *node_record_table_ptr __attribute__((weak_import));
List part_list __attribute__((weak_import));
List job_list __attribute__((weak_import));
int node_record_count __attribute__((weak_import));
time_t last_node_update __attribute__((weak_import));
struct switch_record *switch_record_table __attribute__((weak_import));
int switch_record_cnt __attribute__((weak_import));
bitstr_t *avail_node_bitmap __attribute__((weak_import));
bitstr_t *idle_node_bitmap __attribute__((weak_import));
uint16_t *cr_node_num_cores __attribute__((weak_import));
uint32_t *cr_node_cores_offset __attribute__((weak_import));
#else
slurm_ctl_conf_t slurmctld_conf;
struct node_record *node_record_table_ptr;
List part_list;
List job_list;
int node_record_count;
time_t last_node_update;
struct switch_record *switch_record_table;
int switch_record_cnt;
bitstr_t *avail_node_bitmap;
bitstr_t *idle_node_bitmap;
uint16_t *cr_node_num_cores;
uint32_t *cr_node_cores_offset;
#endif

const char plugin_name[]	= "Consumable Resources RGPU Node Selection plugin";;
const char plugin_type[]	= "select/cons_rgpu";
uint32_t plugin_id		= 200;
const uint32_t plugin_version	= 100;

typedef struct gres_state {
	uint32_t plugin_id;
	void *gres_data;
} gres_state_t;

struct node_record *node_record_table_ptr;  /* ptr to node records */
static int select_node_cnt = 0;

/***************************** RCUDA ******************************************/

static int _call_alloc_rgpu(int *n, int i, int *offset_aux, uint32_t *remaining,
                                int action, struct job_record *job_ptr)
{
	struct job_resources *job = job_ptr->job_resrcs;
	struct node_record *node_ptr;
	List gres_list;
	
	*n += 1;
	offset_aux[*n] = i;
	job->node_offset_list[i] = *n;
	
	if (*remaining>0) {
		node_ptr = &node_record_table_ptr[i];
		if (action!=2) {
			gres_list = node_ptr->gres_list;
			//allocating gpus             
			gres_rgpu_job_alloc(job, gres_list, *n, job_ptr->job_id, 
				node_ptr->name, remaining);
			gres_plugin_node_state_log(gres_list, node_ptr->name);
		} //if action
	}// if remaining
	else {
		//clearing this node from the bitmap since it won't be used
		bit_clear(job->rgpu_node_bitmap, i);
	}
	
	return SLURM_SUCCESS;
}

/*
 * This function determines whether a job is runnable based 
 * on the nodes whose rgpus 
 * have got neither enough memory nor an appropriate compute capability   
 */
int _rgpu_job_is_runnable(uint32_t rgpus_needed,
	uint64_t rmem_min, uint64_t rmem_max,
	uint32_t cc_needed, bitstr_t * bitmap)
{

	ListIterator node_gres_iter;
	gres_state_t *node_gres_ptr;
	gres_node_state_t *gres_node;
	List gres_list;
	struct node_record *node_ptr;
	uint32_t i, size, irgpu, cnt_rgpu_not_avail = 0, total_rgpus = 0;
	size = bit_size(bitmap);
	for (i = 0; i < size; i++) {
		if (!bit_test(bitmap, i))
			continue;
		node_ptr = &node_record_table_ptr[i];
		gres_list = node_ptr->gres_list;
		node_gres_iter = list_iterator_create(gres_list);
		while ((node_gres_ptr = (gres_state_t *) list_next(node_gres_iter))) {
			if (node_gres_ptr->plugin_id==1970300786) {
				gres_node = (gres_node_state_t *) node_gres_ptr->gres_data;
				if (gres_node->gres_cnt_avail > 0) {
					/* For each rgpu in the node:
					 *	Test if its cc is equal or greater than the required
					 *	Test if it has enough memory
					 * -- Getting how many rgpus will be able to host the job --					 
					 */
					for (irgpu = 0; irgpu < gres_node->gres_cnt_avail; irgpu++) {
						if (gres_node->cc_version[irgpu] < cc_needed)
							cnt_rgpu_not_avail += 1;
						else if (gres_node->mem_rgpu_avail[irgpu] < rmem_min) //not enough memory
							cnt_rgpu_not_avail += 1;
					}
				}
				total_rgpus += gres_node->gres_cnt_avail;
			}
		}
		list_iterator_destroy(node_gres_iter);
	} //for
	if (rgpus_needed > total_rgpus){
		error("job_test.c: _rgpu_job_is_runnable(): "
			"There aren't enough rgpus (needed:%u, total:%u)",
			rgpus_needed, total_rgpus);
		return SLURM_ERROR;
	}
	if (rgpus_needed > (total_rgpus - cnt_rgpu_not_avail)){
		error("job_test.c: _rgpu_job_is_runnable(): "
			"There aren't enough appropriate rgpus (needed:%u, total:%u, not appropriate:%u)",
			rgpus_needed, total_rgpus, cnt_rgpu_not_avail);
		return SLURM_ERROR;
	}
	return SLURM_SUCCESS;
}

/*
 * This function clears from the bitmap, the nodes without rgpus
 * or nodes whose rgpus haven't got enough memory.  
 */
int _select_rgpus_nodes(
			struct job_record *job_ptr,
			bitstr_t * bitmap,
			uint32_t rgpus_needed, uint64_t rmem_min, 
			uint64_t rmem_max, uint32_t cc_needed)
{

	ListIterator node_gres_iter;
	gres_state_t *node_gres_ptr;
	gres_node_state_t *gres_node;

	struct node_record *node_ptr;
	uint32_t i, size, cnt_alloc_rgpus = 0, cnt_node_alloc_rgpu = 0, cnt_avail_rgpu = 0;
	int balance_rgpus = 0, irgpu;
	List gres_list;
	bool some_useful_rgpu;
	
	size = bit_size(bitmap);
	for (i = 0; i<size; i++) {
		if (!bit_test(bitmap, i))
			continue;
		node_ptr = &node_record_table_ptr[i];
		gres_list = node_ptr->gres_list;
		cnt_node_alloc_rgpu = 0;
		some_useful_rgpu = false;
		node_gres_iter = list_iterator_create(gres_list);
		while ((node_gres_ptr = (gres_state_t *) list_next(node_gres_iter))) {
			if (node_gres_ptr->plugin_id==1970300786) {
				gres_node = (gres_node_state_t *) node_gres_ptr->gres_data;
				if (gres_node->gres_cnt_avail>0) {
					for (irgpu = 0; irgpu<gres_node->gres_cnt_avail; irgpu++) {
						//debug("\tRCUDA %s(%s,%d) - - node=%s i=%i mem_rgpu_alloc[i]=%lu mem_rgpu_avail=%lu", 
						//	__FILE__, __func__, __LINE__, node_ptr->name, irgpu, gres_node->mem_rgpu_alloc[irgpu], gres_node->mem_rgpu_avail[irgpu]);
						if (gres_node->cc_version[irgpu] < cc_needed)
							cnt_node_alloc_rgpu++;
						
						else if ((gres_node->mem_rgpu_avail[irgpu] - gres_node->mem_rgpu_alloc[irgpu]) < rmem_min) //not enough memory
							cnt_node_alloc_rgpu++;

						else if (rmem_max == 0) // exclusive mode
							if (gres_node->mem_rgpu_alloc[irgpu] > 0) //it's being shared
								cnt_node_alloc_rgpu++;
					}
					cnt_alloc_rgpus += cnt_node_alloc_rgpu;
					cnt_avail_rgpu += gres_node->gres_cnt_avail;
					if ((gres_node->gres_cnt_avail - cnt_node_alloc_rgpu)>0)
						some_useful_rgpu = true;
					break;
				}
			}
		}
		list_iterator_destroy(node_gres_iter);
		
		if (!some_useful_rgpu)
			goto clear_bit;
		else
			continue;

clear_bit: /* This node is not usable by this job */
		bit_clear(bitmap, i);
		/* if the set bits aren't enough to satisfy the job requeriments
		 * and, in the selected nodes(partition) there aren't rgpus, 
		 * the job will never be runnable */
		if (cnt_avail_rgpu == 0 && bit_set_count(bitmap) == 0) {
			error("job_test.c: _select_rgpus_nodes(): There aren't rgpus in the selected nodes");
			return -2;
		}
	} // for
	
	if (rgpus_needed==0)	
		return SLURM_ERROR;

	balance_rgpus = cnt_alloc_rgpus+rgpus_needed-cnt_avail_rgpu;
	if (balance_rgpus>0) {
		error("job_test.c: _select_rgpus_nodes(): There aren't enough "
			"available rgpus at the moment - allocated:%u, needed:%u, total:%u",
			cnt_alloc_rgpus, rgpus_needed, cnt_avail_rgpu);
		return SLURM_ERROR;
	} else {
		debug("job_test.c: _select_rgpus_nodes(): There are "
			"available rgpus at the moment - allocated:%u, needed:%u, total:%u",
			cnt_alloc_rgpus, rgpus_needed, cnt_avail_rgpu);
	}
	return SLURM_SUCCESS;
}

/******************************************************************************/

extern int init ( void )
{
	verbose("%s loaded", plugin_name);
	return other_select_init();
}

extern int fini ( void )
{
	return other_select_fini();
}

extern int select_p_node_init(struct node_record *node_ptr, int node_cnt)
{
	node_record_table_ptr = node_ptr;
	select_node_cnt = node_cnt;
	return other_node_init(node_ptr, node_cnt);
}

extern int select_p_block_init(List part_list)
{
	return other_block_init(part_list);
}

extern int select_p_state_restore(char *dir_name)
{
	return other_state_restore(dir_name);
}

extern int select_p_job_init(List job_list)
{
	return other_job_init(job_list);
}

extern int select_p_job_test(struct job_record *job_ptr, bitstr_t *bitmap,
			     uint32_t min_nodes, uint32_t max_nodes,
			     uint32_t req_nodes, uint16_t mode,
			     List preemptee_candidates,
			     List *preemptee_job_list,
			     bitstr_t *exc_core_bitmap)
{
	int rc, error_code, enqueue = 0;
	job_resources_t *job_res;
	bitstr_t * bitmap_rgpus_nodes = bit_copy(bitmap);
	gres_state_t *job_gres_ptr;
	uint32_t rgpus_needed = 0, cc_needed = 0;
	uint64_t rmem_needed = 0, rmem_min = 0, rmem_max = 0;
	gres_state_t *gres_state;
	gres_job_state_t *gres_job_state;
	uint16_t rcuda_mode, rcuda_dist, rcuda_mem;
	char *temp_str1, *temp_str2;
	
	rc = other_job_test(job_ptr, bitmap, min_nodes, max_nodes,
		      req_nodes, mode, preemptee_candidates,
		      preemptee_job_list, exc_core_bitmap);
	
	if (rc == SLURM_ERROR)
		return SLURM_ERROR;
	
	if (! job_ptr->rgpu_enable) {
		job_ptr->rgpu_enable = 0;
		debug("cons_rgpu/job_test.c: cr_job_test(): job_id: %i running "
			"in original mode (bit_set_count(node_bitmap)=%u)",
			job_ptr->job_id, bit_set_count(bitmap));
		
			if (mode == SELECT_MODE_RUN_NOW) {
				job_res = job_ptr->job_resrcs;
				job_res->rgpu_node_bitmap = NULL;
				job_res->rgpus = 0;
				job_res->rgpu_job_state = NULL;
			}
	} else {
		gres_state = xmalloc(sizeof(gres_state_t));
		gres_job_state = xmalloc(sizeof(gres_job_state_t));
		job_gres_ptr = (gres_state_t *) job_ptr->gres_rgpu;
		gres_job_state = (gres_job_state_t *) job_gres_ptr->gres_data;

		//Getting the minimun compute capability
		cc_needed = gres_job_state->com_cap;
		//Getting the max and min memory needed
		rmem_needed = gres_job_state->rgpu_cnt_mem;
		
		/* Determining how the rgpus will be selected */
		rcuda_mode = slurmctld_conf.rcuda_mode;
		if (job_ptr->rcuda_mode)
			rcuda_mode = job_ptr->rcuda_mode;
		rcuda_dist = slurmctld_conf.rcuda_dist;
		if (job_ptr->rcuda_dist)
			rcuda_dist = job_ptr->rcuda_dist;
		rcuda_mem = slurmctld_conf.rgpu_min_mem;
		if (rmem_needed)
			rcuda_mem = rmem_needed;
		if (rcuda_mode == 1) temp_str1 = xstrdup("Exclusive");
		else temp_str1 = xstrdup("Shared");
		if (rcuda_dist == 1) temp_str2 = xstrdup("Global");
		else temp_str2 = xstrdup("Node");
		debug("cons_rgpu/job_test.c: cr_job_test(): job_id: %i will "
			"be scheduled with mode %s and distribution %s "
			"(min %i MB)", job_ptr->job_id, temp_str1, 
			temp_str2, rcuda_mem);
		xfree(temp_str1);
		xfree(temp_str2);
		
		if (rcuda_mode == 1) { //exclusive mode
			rmem_max = 0; //all the memory of the rgpu
			rmem_min = slurmctld_conf.rgpu_min_mem;
			if (rmem_needed > 0)
				rmem_min = rmem_needed;
		} else { //shared mode
			if (rmem_needed > 0) {
				rmem_max = rmem_needed;
				rmem_min = rmem_needed;
			} else {
				rmem_max = slurmctld_conf.rgpu_min_mem;
				rmem_min = slurmctld_conf.rgpu_min_mem;
			}
		}
		
		//Getting the total amount of rgpus needed
		rgpus_needed = gres_job_state->gres_cnt_alloc; //global
		if (rcuda_dist == 2) //per node
			rgpus_needed = rgpus_needed * req_nodes; 
		
		error_code = _rgpu_job_is_runnable(rgpus_needed, rmem_min, 
			rmem_max, cc_needed, bitmap_rgpus_nodes);
					
		if (error_code == SLURM_ERROR) {
			debug("cons_rgpu/job_test.c: cr_job_test(): job_id: %i "
				"impossible to run", job_ptr->job_id);
			return SLURM_ERROR;
		}
		/* if the error code of _select_rgpus_nodes() is -1, the job 
		 * will be queued */
		error_code = _select_rgpus_nodes(job_ptr, bitmap_rgpus_nodes,
			rgpus_needed, rmem_min, rmem_max, cc_needed);
		if (error_code == SLURM_ERROR) {
			debug("cons_rgpu/job_test.c: cr_job_test(): job_id: %i "
				"is going to be queued", job_ptr->job_id);
			rc = SLURM_SUCCESS;
			enqueue = 1;
		}
		else if (error_code == -2) {
			/* job cannot fit */
			debug("cons_rgpu/job_test.c: cr_job_test(): job_id: "
				"%i: insufficient rgpu resources in the "
				"partition", job_ptr->job_id);
			rc = SLURM_ERROR;
		}
	
		/**************************************************************/
		
		if (mode == SELECT_MODE_RUN_NOW) {
			if (rc == SLURM_SUCCESS) {
				if (enqueue) {
					rc = SLURM_ERROR;
				} else {
				job_res = job_ptr->job_resrcs;
				job_res->rgpu_node_bitmap = bit_copy(bitmap_rgpus_nodes);
				job_res->rgpus = rgpus_needed;
				job_res->memrgpu = rmem_min;			//deprecated
				job_res->rgpu_job_state = job_ptr->gres_rgpu;
				job_res->rgpumem_min = rmem_min;
				job_res->rgpumem_max = rmem_max;
				job_res->rgpucc = cc_needed;
				}
			}
		}
	}	
	return rc;
}

extern int select_p_job_begin(struct job_record *job_ptr)
{
	return other_job_begin(job_ptr);
}

extern int select_p_job_ready(struct job_record *job_ptr)
{
	return other_job_ready(job_ptr);
}

extern int select_p_job_fini(struct job_record *job_ptr)
{
	struct job_resources *job = job_ptr->job_resrcs;
	struct node_record *node_ptr;
	int i, n;
	List gres_list;

	if (!job || !job->core_bitmap) {
		error("job %u has no job_resrcs info", job_ptr->job_id);
		return SLURM_ERROR;
	}

	if (job_ptr->rgpu_enable) { //rgpu mode
		debug("cons_rgpu: _rm_job_from_res: deallocating: %u rgpus within %u nodes",
			job->rgpus, bit_set_count(job->rgpu_node_bitmap));
		for (i=0; i<select_node_cnt; i++){
			if (!bit_test(job->rgpu_node_bitmap, i))
				continue;
			n = job->node_offset_list[i];
			node_ptr = &node_record_table_ptr[i];
			gres_list = node_ptr->gres_list;
			gres_rgpu_job_dealloc(job->rgpu_job_state, gres_list,
				n, job_ptr->job_id, node_ptr->name);
			//gres_plugin_node_state_log(gres_list, node_ptr->name);
		}
	}//end if rgpu mode

	return other_job_fini(job_ptr);
}

extern int select_p_state_save(char *dir_name)
{
	return other_state_save(dir_name);
}

extern bool select_p_node_ranking(struct node_record *node_ptr, int node_cnt)
{
	return false;
}

extern int select_p_job_resized(struct job_record *job_ptr,
				struct node_record *node_ptr)
{
	return other_job_resized(job_ptr, node_ptr);
}

extern bool select_p_job_expand_allow(void)
{
	return other_job_expand_allow();
}

extern int select_p_job_expand(struct job_record *from_job_ptr,
			       struct job_record *to_job_ptr)
{
	return other_job_expand(from_job_ptr, to_job_ptr);
}

extern int select_p_job_signal(struct job_record *job_ptr, int signal)
{
	return other_job_signal(job_ptr, signal);
}

extern int select_p_job_suspend(struct job_record *job_ptr, bool indf_susp)
{
	return other_job_suspend(job_ptr, indf_susp);
}

extern int select_p_job_resume(struct job_record *job_ptr, bool indf_susp)
{
	return other_job_resume(job_ptr, indf_susp);
}

extern bitstr_t *select_p_step_pick_nodes(struct job_record *job_ptr,
					  select_jobinfo_t *step_jobinfo,
					  uint32_t node_count,
					  bitstr_t **avail_nodes)
{
	return other_step_pick_nodes(job_ptr, step_jobinfo, node_count, 
				     avail_nodes);
}

extern int select_p_step_start(struct step_record *step_ptr)
{
	return other_step_start(step_ptr);
}

extern int select_p_step_finish(struct step_record *step_ptr)
{
	return other_step_finish(step_ptr);
}

extern int select_p_pack_select_info(time_t last_query_time,
				     uint16_t show_flags, Buf *buffer_ptr,
				     uint16_t protocol_version)
{
	return other_pack_select_info(last_query_time, show_flags, buffer_ptr,
				      protocol_version);
}

extern select_nodeinfo_t *select_p_select_nodeinfo_alloc(void)
{
	return other_select_nodeinfo_alloc();
}

extern int select_p_select_nodeinfo_free(select_nodeinfo_t *nodeinfo)
{
	return other_select_nodeinfo_free(nodeinfo);
}

extern int select_p_select_nodeinfo_pack(select_nodeinfo_t *nodeinfo,
					 Buf buffer, uint16_t protocol_version)
{
	return other_select_nodeinfo_pack(nodeinfo,
					buffer, protocol_version);
}

extern int select_p_select_nodeinfo_unpack(select_nodeinfo_t **nodeinfo_pptr,
					   Buf buffer,
					   uint16_t protocol_version)
{
	return other_select_nodeinfo_unpack(nodeinfo_pptr,
					  buffer, protocol_version);
}

extern int select_p_select_nodeinfo_set_all(void)
{
	return other_select_nodeinfo_set_all();
}

extern int select_p_select_nodeinfo_set(struct job_record *job_ptr)
{
	int action = 0;
	struct job_resources *job = job_ptr->job_resrcs;
	struct node_record *node_ptr;
	int i, n, j;

	gres_state_t *job_gres_ptr;
	gres_job_state_t *job_gres_data;
	char *dev_list = NULL;
	
	int offset_aux[select_node_cnt];
	job->node_offset_list = malloc(select_node_cnt * sizeof(int));

	if (!job || !job->core_bitmap) {
		error("job %u has no job_resrcs info", job_ptr->job_id);
		return SLURM_ERROR;
	}
	
	if (job_ptr->rgpu_enable) {
		debug("cons_rgpu: select_p_select_nodeinfo_set: rgpus needed: %u", job->rgpus);
		uint32_t remaining = job->rgpus;

		/*
		 * First, we try to allocate rgpus from the selected nodes
		 */
		n = -1;
		for (i = 0; i<select_node_cnt; i++) {
			if (!bit_test(job_ptr->node_bitmap, i) || !bit_test(job->rgpu_node_bitmap, i))
				continue;
			_call_alloc_rgpu(&n, i, offset_aux, &remaining, action, job_ptr);
		}
		
		/* 
		 * Then, if the job needs more rgpus, we will find them through
		 * the rest of the nodes.
		 */
		for (i = 0; i<select_node_cnt; i++) { //iterating the nodes
			if (!bit_test(job->rgpu_node_bitmap, i) || bit_test(job_ptr->node_bitmap, i))
				continue;
			_call_alloc_rgpu(&n, i, offset_aux, &remaining, action, job_ptr);
		}

		//collecting information about allocated gpus
		job_gres_ptr = (gres_state_t *) job->rgpu_job_state;
		job_gres_data = (gres_job_state_t *) job_gres_ptr->gres_data;
		for (i = 0, n = -1; i<select_node_cnt; i++) { //recorre los nodos
			if (!bit_test(job->rgpu_node_bitmap, i))
				continue;
			n++;
			if (job_gres_data->gres_bit_alloc[n]!=NULL) {
				for (j = 0; j<bit_size(job_gres_data->gres_bit_alloc[n]); j++) { //recorre las gpus
					if (bit_test(job_gres_data->gres_bit_alloc[n], j)) {
						if (!dev_list)
							dev_list = xmalloc(128);
						else
							xstrcat(dev_list, ",");
						node_ptr = &node_record_table_ptr[offset_aux[n]];
						xstrfmtcat(dev_list, "%s:%i", node_ptr->name, j);
					}
				} //for gpus
			}// if not null
		} //for nodes
		xstrcat(dev_list, "\0");
		job_ptr->rgpu_list = xstrdup(dev_list);
		job_ptr->rgpu_alloc_list = bit_alloc(job->rgpus);
		debug("cons_rgpu: _add_job_to_res: job id: %u - number gpus: %u - rgpulist: %s - nodelist: %s",
			job_ptr->job_id, job->rgpus, job_ptr->rgpu_list, job_ptr->nodes);
		xfree(dev_list);
	} //if rgpus mode

	return other_select_nodeinfo_set(job_ptr);
}

extern int select_p_select_nodeinfo_get(select_nodeinfo_t *nodeinfo,
					enum select_nodedata_type dinfo,
					enum node_states state,
					void *data)
{
	return other_select_nodeinfo_get(nodeinfo, dinfo, state, data);
}

extern select_jobinfo_t *select_p_select_jobinfo_alloc(void)
{
	return other_select_jobinfo_alloc();
}

extern int select_p_select_jobinfo_set(select_jobinfo_t *jobinfo,
				       enum select_jobdata_type data_type,
				       void *data)
{
	return other_select_jobinfo_set(jobinfo, data_type, data);
}

extern int select_p_select_jobinfo_get(select_jobinfo_t *jobinfo,
				       enum select_jobdata_type data_type,
				       void *data)
{
	return other_select_jobinfo_get(jobinfo, data_type, data);
}

extern select_jobinfo_t *select_p_select_jobinfo_copy(select_jobinfo_t *jobinfo)
{
	return other_select_jobinfo_copy(jobinfo);
}

extern int select_p_select_jobinfo_free(select_jobinfo_t *jobinfo)
{
	return other_select_jobinfo_free(jobinfo);
}

extern int select_p_select_jobinfo_pack(select_jobinfo_t *jobinfo, Buf buffer,
					uint16_t protocol_version)
{
	return other_select_jobinfo_pack(jobinfo, buffer, protocol_version);
}

extern int select_p_select_jobinfo_unpack(select_jobinfo_t **jobinfo_pptr,
					  Buf buffer, uint16_t protocol_version)
{
	return other_select_jobinfo_unpack(jobinfo_pptr, buffer, 
					   protocol_version);
}

extern char *select_p_select_jobinfo_sprint(select_jobinfo_t *jobinfo,
					    char *buf, size_t size, int mode)
{
	return other_select_jobinfo_sprint(jobinfo, buf, size, mode);
}

extern char *select_p_select_jobinfo_xstrdup(select_jobinfo_t *jobinfo,
					     int mode)
{
	return other_select_jobinfo_xstrdup(jobinfo, mode);
}

extern int select_p_update_block(update_block_msg_t *block_desc_ptr)
{
	return other_update_block(block_desc_ptr);
}

extern int select_p_update_sub_node(update_block_msg_t *block_desc_ptr)
{
	return other_update_sub_node(block_desc_ptr);
}

extern int select_p_fail_cnode(struct step_record *step_ptr)
{
	return other_fail_cnode(step_ptr);
}

extern int select_p_get_info_from_plugin(enum select_jobdata_type info,
					 struct job_record *job_ptr,
					 void *data)
{
	return other_get_info_from_plugin(info, job_ptr, data);
}

extern int select_p_update_node_config(int index)
{
	return other_update_node_config(index);
}

extern int select_p_update_node_state(struct node_record *node_ptr)
{
	return other_update_node_state(node_ptr);
}

extern int select_p_alter_node_cnt(enum select_node_cnt type, void *data)
{
	return other_alter_node_cnt(type, data);
}

extern int select_p_reconfigure(void)
{
	return other_reconfigure();
}

extern bitstr_t * select_p_resv_test(resv_desc_msg_t *resv_desc_ptr,
				     uint32_t node_cnt,
				     bitstr_t *avail_bitmap,
				     bitstr_t **core_bitmap)
{
	return other_resv_test(resv_desc_ptr, node_cnt,
			       avail_bitmap, core_bitmap);
}

extern void select_p_ba_init(node_info_msg_t *node_info_ptr, bool sanity_check)
{
	other_ba_init(node_info_ptr, sanity_check);
}

extern int *select_p_ba_get_dims(void)
{
	other_ba_get_dims();
}

extern void select_p_ba_fini(void)
{
	other_ba_fini();
}

extern bitstr_t *select_p_ba_cnodelist2bitmap(char *cnodelist)
{
	return other_ba_cnodelist2bitmap(cnodelist);
}